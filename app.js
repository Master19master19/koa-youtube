'use strict';
var main = require('./controllers/main');
var compress = require('koa-compress');
var route = require('koa-route');
var koa = require('koa');
var path = require('path');
var app = module.exports = new koa();


app.use(route.get('/', main.home));
app.use(route.get('/get/:query', main.fetch));


// Compress
app.use(compress());

if (!module.parent) {
  app.listen(1337);
  console.log('listening on port 1337');
}
