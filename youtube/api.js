const fs = require('fs');
const ytdl = require('ytdl-core');
// TypeScript: import ytdl from 'ytdl-core'; with --esModuleInterop
// TypeScript: import * as ytdl from 'ytdl-core'; with --allowSyntheticDefaultImports
// TypeScript: import ytdl = require('ytdl-core'); with neither of the above

// 4k - 2160p - 313
// HDReady - 720p - 136
// fullHD - 1080p - 137
// web360 - 360p - 134

// Format found! 360p 18
// Format found! 360p 43
// Format found! 360p 134
// Format found! 360p 243
// Format found! 720p 247
// Format found! 720p 22
// Format found! 720p 136
// Format found! 1080p 137 fullHD
// 4 формата (4к, fullHD, HDReady, web360)


let qualities = {
	137: "fhd",
	136: "hd" ,
	134: "360",
	// 313: "4k"
};

let url = 'https://www.youtube.com/watch?v=EOGp4TscVrE';

ytdl.getInfo( url , ( err , info ) => {
	if ( err ) throw err;
	for ( el in qualities ) {
		let format = ytdl.chooseFormat( info.formats , { quality: el });
		if ( undefined != format.type ) {
			// console.log('Format found!', format.resolution,el);
			ytdl( url , { 'quality' : el } )
			  .pipe( fs.createWriteStream( `video-${qualities[el]}.mp4` ) );
		}
	};
});


module.exports.trace = function *() {
  this.body = "Smart! But you can't trace.";
};
