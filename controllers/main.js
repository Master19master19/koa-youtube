'use strict';
// var parse = require('co-body');
var co = require('co');

var youtube = require('../youtube/api');

// From lifeofjs
co(function * () {
  var books = yield 'life';
});

module.exports.fetch = function * fetch(query,next) {
  if ('GET' != this.method) return yield next;
  this.body = yield {'query':query};
};